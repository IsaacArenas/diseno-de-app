import React, { useEffect, useState } from 'react';
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import ResolvedScreen from './Screens/ResolvedScreen';
import TodosScreen from './Screens/TodosScreen';
import UnresolvedScreen from './Screens/UnresolvedScreen';

const App = () => {
  const [todos, setTodos] = useState([]);
  const [activeScreen, setActiveScreen] = useState('todos');

  useEffect(() => {
    fetchData();
  }, []);

  const fetchData = async () => {
    try {
      const response = await fetch('http://jsonplaceholder.typicode.com/todos');
      const data = await response.json();
      setTodos(data);
    } catch (error) {
      console.error('Error fetching data:', error);
    }
  };

  const renderScreen = () => {
    switch (activeScreen) {
      case 'todos':
        return <TodosScreen todos={todos} />;
      case 'unresolved':
        return <UnresolvedScreen todos={todos} />;
      case 'resolved':
        return <ResolvedScreen todos={todos} />;
      default:
        return null;
    }
  };

  const renderMenu = () => (
    <View style={styles.menuContainer}>
      <View style={styles.menu}>
        <TouchableOpacity onPress={() => setActiveScreen('todos')} style={styles.menuButton}>
          <Text style={styles.menuText}>Todos</Text>
        </TouchableOpacity>
        <TouchableOpacity onPress={() => setActiveScreen('unresolved')} style={styles.menuButton}>
          <Text style={styles.menuText}>Sin Resolver</Text>
        </TouchableOpacity>
        <TouchableOpacity onPress={() => setActiveScreen('resolved')} style={styles.menuButton}>
          <Text style={styles.menuText}>Resueltos</Text>
        </TouchableOpacity>
      </View>
    </View>
  );

  return (
    <View style={styles.container}>
      {renderScreen()}
      {renderMenu()}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#f0f0f0',
  },
  menuContainer: {
    marginTop: 20, // Ajusta este valor según sea necesario
  },
  menu: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
    backgroundColor: '#ccc',
    paddingVertical: 10,
  },
  menuButton: {
    paddingHorizontal: 20,
    paddingVertical: 10,
    backgroundColor: '#eee',
    borderRadius: 10,
  },
  menuText: {
    fontSize: 16,
    fontWeight: 'bold',
  },
});

export default App;
