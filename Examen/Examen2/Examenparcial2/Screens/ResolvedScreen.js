import React from 'react';
import { ScrollView, StyleSheet, Text, View } from 'react-native';

const ResolvedScreen = ({ todos }) => {
  const resolvedTodos = todos.filter(todo => todo.completed);

  const renderTodoList = (todosList) => (
    <View style={styles.todoListContainer}>
      {todosList.map(todo => (
        <View key={todo.id} style={styles.todoItem}>
          <Text style={styles.todoTitle}>{todo.title}</Text>
          <Text style={styles.todoDetail}>ID: {todo.id} | Usuario: {todo.userId}</Text>
        </View>
      ))}
    </View>
  );

  return (
    <ScrollView contentContainerStyle={styles.scrollContainer}>
      <Text style={styles.subheading}>Pendientes Resueltos (ID y Título)</Text>
      {renderTodoList(resolvedTodos)}
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  scrollContainer: {
    paddingHorizontal: 20,
  },
  subheading: {
    fontSize: 18,
    fontWeight: 'bold',
    marginTop: 20,
    textAlign: 'center',
  },
  todoListContainer: {
    borderWidth: 1,
    borderColor: '#ccc',
    borderRadius: 5,
    padding: 10,
    marginTop: 10,
    backgroundColor: '#fff',
  },
  todoItem: {
    marginBottom: 10,
  },
  todoTitle: {
    fontSize: 16,
    marginBottom: 5,
  },
  todoDetail: {
    fontSize: 14,
    color: '#666',
  },
});

export default ResolvedScreen;
