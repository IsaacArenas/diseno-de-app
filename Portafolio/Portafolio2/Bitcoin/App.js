import React from 'react';
import { StatusBar } from 'expo-status-bar';
import { StyleSheet, View } from 'react-native';
import BtcUsd from './components/BtcUsd';
import BtcEuro from './components/BtcEuro';
import BtcGbp from './components/BtcGbp';


export default function App() {
  return (
    <View style={styles.container}>
      <View style={styles.box}>
        <BtcUsd />
      </View>
      <View style={styles.box}>
        <BtcEuro />
      </View>
      <View style={styles.box}>
        <BtcGbp />
      </View>
      <StatusBar style="auto" />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  box: {
    width: '90%',
    backgroundColor: 'white',
    borderRadius: 10,
    padding: 20,
    marginBottom: 10,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
});
