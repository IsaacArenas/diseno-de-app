import React, { useState, useEffect, useRef } from 'react';
import { View, Text, StyleSheet, TouchableOpacity, Alert } from 'react-native';
import * as Print from 'expo-print';
import * as Sharing from 'expo-sharing';
import { useFocusEffect } from '@react-navigation/native';
import DrawerSceneWrapper from '../../components/menu/DrawerSceneWrapper';
import { Ionicons } from '@expo/vector-icons';
import API_URL from '../../apiConfig';

const CLIENT_ID = 43; // Número de cliente

export default function InvoiceDetails({ navigation }) {
  const [invoices, setInvoices] = useState([]);
  const [clientRFC, setClientRFC] = useState('');
  const [loading, setLoading] = useState(true);
  const titleRef = useRef(null); 

  useEffect(() => {
    const fetchClientRFC = async () => {
      try {
        const response = await fetch(`${API_URL}/cliente/${CLIENT_ID}`);
        if (!response.ok) {
          throw new Error('Failed to fetch client data');
        }
        const clientData = await response.json();
        setClientRFC(clientData.rfc);
      } catch (error) {
        console.error(error);
      }
    };

    const fetchInvoiceData = async () => {
      try {
        const response = await fetch(`${API_URL}/factura/cliente${CLIENT_ID}`);
        if (!response.ok) {
          throw new Error('Failed to fetch invoice data');
        }
        const invoiceData = await response.json();
        setInvoices(invoiceData);
        setLoading(false);
      } catch (error) {
        console.error(error);
        setLoading(false);
      }
    };
    
    fetchClientRFC();
    fetchInvoiceData();
  }, []);

  useFocusEffect(
    React.useCallback(() => {
      if (titleRef.current) {
        titleRef.current.fadeInDown(1000);
      }
    }, [])
  );

  const handlePay = async () => {
    try {
      let contentHTML = `
      <html>
        <head>
          <style>
            body { font-family: Arial, sans-serif; text-align: center; }
            h1 { font-size: 24px; margin-bottom: 10px; }
            p { margin-bottom: 5px; }
            .invoice-item { margin-bottom: 20px; }
            .invoice-item-heading { font-size: 18px; font-weight: bold; }
            .invoice-item-details { margin-left: 10px; }
          </style>
        </head>
        <body>
          <h1>Eventpro</h1>
          <h2>Receipt</h2>
          <p>Client RFC: ${clientRFC}</p>
          <hr/>
      `;

      invoices.forEach((invoice, index) => {
        contentHTML += `
          <div class="invoice-item">
            <div class="invoice-item-heading">Invoice #${index + 1}</div>
            <div class="invoice-item-details">
              <p><strong>Description:</strong> ${invoice.descripcion}</p>
              <p><strong>Date:</strong> ${invoice.fecha_factura}</p>
              <p><strong>Total Amount:</strong> ${invoice.monto_total}</p>
              <p><strong>Subtotal:</strong> ${invoice.sub_total}</p>
              <p><strong>IVA:</strong> ${invoice.IVA}</p>
              <p><strong>Payment Method:</strong> Card</p>
            </div>
          </div>
        `;
      });

      contentHTML += `
        </body>
      </html>
      `;

      const pdf = await Print.printToFileAsync({ html: contentHTML });
      if (pdf.uri) {
        await Sharing.shareAsync(pdf.uri);
      } else {
        throw new Error('Failed to generate PDF');
      }
    } catch (error) {
      console.error('Error al generar el PDF:', error);
      Alert.alert('Error al generar el PDF');
    }
  };

  if (loading) {
    return (
      <View style={styles.container}>
        <Text>Loading...</Text>
      </View>
    );
  }

  if (invoices.length === 0) {
    return (
      <View style={styles.container}>
        <Text>No invoice data available</Text>
      </View>
    );
  }

  return (
    <DrawerSceneWrapper>
      <View style={styles.container}>
        <TouchableOpacity onPress={() => navigation.openDrawer()} style={styles.menuButton}>
          <Ionicons name="menu" size={26} color="#009688" />
        </TouchableOpacity>
        <Text ref={titleRef} style={styles.title}>My Payments</Text>
        <View style={styles.invoiceContainer}>
          <Text style={styles.invoiceTitle}>EVENTPRO</Text>
          <Text style={styles.clientInfo}>Client RFC: {clientRFC}</Text>
          {invoices.map((invoice, index) => (
            <View key={index} style={styles.invoiceItem}>
              <Text style={styles.invoiceItemHeading}>Invoice #{index + 1}</Text>
              <View style={styles.invoiceItemDetails}>
                <Text><Text style={styles.boldText}>Description:</Text> {invoice.descripcion}</Text>
                <Text><Text style={styles.boldText}>Date:</Text> {invoice.fecha_factura}</Text>
                <Text><Text style={styles.boldText}>Total Amount:</Text> ${invoice.monto_total}</Text>
                <Text><Text style={styles.boldText}>Subtotal:</Text> ${invoice.sub_total}</Text>
                <Text><Text style={styles.boldText}>IVA:</Text> ${invoice.IVA}</Text>
                <Text><Text style={styles.boldText}>Payment Method:</Text> Card</Text>
              </View>
            </View>
          ))}
        </View>
        <TouchableOpacity style={styles.payButton} onPress={handlePay}>
          <Text style={styles.payButtonText}>Pay</Text>
        </TouchableOpacity>
      </View>
    </DrawerSceneWrapper>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'white',
    padding: 20,
  },
  title: {
    fontSize: 24,
    fontWeight: 'bold',
    marginBottom: 20,
  },
  menuButton: {
    position: 'absolute',
    top: 35,
    left: 20,
    zIndex: 1,
  },
  invoiceContainer: {
    borderWidth: 1,
    borderColor: '#ccc',
    borderRadius: 10,
    padding: 20,
    marginBottom: 20,
    alignItems: 'center',
  },
  invoiceTitle: {
    fontSize: 24,
    fontWeight: 'bold',
    marginBottom: 5,
  },
  clientInfo: {
    fontSize: 16,
    marginBottom: 10,
  },
  invoiceItem: {
    marginBottom: 20,
    width: '100%',
  },
  invoiceItemHeading: {
    fontSize: 18,
    fontWeight: 'bold',
    marginBottom: 5,
    textAlign: 'left',
  },
  invoiceItemDetails: {
    marginLeft: 10,
    textAlign: 'left',
  },
  boldText: {
    fontWeight: 'bold',
  },
  payButton: {
    backgroundColor: 'black',
    padding: 10,
    borderRadius: 5,
    alignSelf: 'stretch',
  },
  payButtonText: {
    color: 'white',
    fontWeight: 'bold',
    textAlign: 'center',
  },
});
