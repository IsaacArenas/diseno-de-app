const USERS = {
  1: {
    id: 1,
    username: 'Alexia Jane',
    avatar: require('../assets/images/users/32.jpeg'),
  },
  2: {
    id: 2,
    username: 'Jacky Depp',
    avatar: require('../assets/images/users/35.jpeg'),
  },
};

const REVIEWS = {
  1: {
    id: 1,
    date: '21 May, 2022',
    author: USERS[1],
    rating: 7,
    text: 'Lorem ipsum dolor sit amet. Iusto nihil et porro soluta ut labore nesciunt sed dolor nihil qui laudantium consequatur',
  },
  2: {
    id: 2,
    date: '14 July, 2021',
    author: USERS[2],
    rating: 9.1,
    text: 'Lorem ipsum dolor sit amet.',
  },
};
export const HOTELS = {
  1: {
    id: 1,
    title: 'Extended Suites Macroplaza',
    image: require('../assets/hoteles/1.1.jpg'),
    location: 'Tijuana',
    rating: 9,
    pricePeerDay: '130$',
    type: 'HOTEL',
    latitude: 32.49035793025243,
    longitude: -116.93110944906195
  },
  2: {
    id: 2,
    title: 'Grand Hotel Tijuana',
    image: require('../assets/hoteles/2.jpg'),
    location: 'Tijuana',
    rating: 9.3,
    pricePeerDay: '230$',
    type: 'HOTEL',
    latitude: 32.5129885577541,
    longitude: -117.00752422822741
  },
  3: {
    id: 3,
    title: 'Hotel Fiesta Inn Tijuana Airport',
    image: require('../assets/hoteles/3.jpg'),
    location: 'Tijuana',
    rating: 9.4,
    pricePeerDay: '280$',
    type: 'HOTEL',
    latitude: 32.53047971382331,
    longitude: -116.95569607365024
  },
  4: {
    id: 4,
    title: 'Hotel Coral y Marina',
    image: require('../assets/hoteles/4.jpg'),
    location: 'Ensenada',
    rating: 9.3,
    pricePeerDay: '190$',
    type: 'HOTEL',
    latitude: 31.865968644712517,
    longitude: -116.6626537795139
  },
  5: {
    id: 5,
    title: "Maglén Resort",
    image: require('../assets/hoteles/5.1.jpg'),
    location: 'Ensenada',
    rating: 9.2,
    pricePeerDay: '250$',
    type: 'HOTEL',
    latitude: 31.99407717164971,
    longitude: -116.63484475775638
  },
  6: {
    id: 6,
    title: 'Torre Lucerna Hotel',
    image: require('../assets/hoteles/6.1.jpg'),
    location: 'Ensenada',
    rating: 9.4,
    pricePeerDay: '270$',
    type: 'HOTEL',
    latitude: 31.86355977808551,
    longitude: -116.65550244489029
  },
  7: {
    id: 7,
    title: 'Santuario Diegueño',
    image: require('../assets/hoteles/7.1.jpg'),
    location: 'Tecate',
    rating: 9.2,
    pricePeerDay: '210$',
    type: 'HOTEL',
    latitude: 32.57632783329583,
    longitude: -116.61982870433351
  },
  8: {
    id: 8,
    title: 'Estancia Inn',
    image: require('../assets/hoteles/8.1.jpg'),
    location: 'Tecate',
    rating: 9.4,
    pricePeerDay: '430$',
    type: 'HOTEL',
    latitude: 32.56419954139376,
    longitude: -116.65170214980198
  },
  9: {
    id: 9,
    title: 'Casa Playa Baja Resort',
    image: require('../assets/hoteles/9.1.jpg'),
    location: 'Rosarito',
    rating: 9.2,
    pricePeerDay: '330$',
    type: 'HOTEL',
    latitude: 32.2255834446609,
    longitude: -116.92081385256097
  },
  10: {
    id: 10,
    title: 'Castilos del Mar',
    image: require('../assets/hoteles/10.1.jpg'),
    location: 'Rosarito',
    rating: 9.4,
    pricePeerDay: '350$',
    type: 'HOTEL',
    latitude: 32.31922607254595,
    longitude: -117.04888230548056
  },
  11: {
    id: 11,
    title: 'Bobbys By The Sea',
    image: require('../assets/hoteles/11.jpg'),
    location: 'Rosarito',
    rating: 9.2,
    pricePeerDay: '230$',
    type: 'HOTEL',
    latitude: 32.38839945381564,
    longitude: -117.05709207084462
  },
};


export const TOP_PLACES = [
  {
    id: 1,
    image: require('../assets/Tijuana/1.jpeg'),
    title: 'Tijuana',
    location: 'Baja California',
    description:
    'Tijuana, in Baja California, Mexico, is a lively border city known for its vibrant nightlife, diverse food scene, and proximity to the US border. It boasts beaches, emerging art, and a rich history, making it a captivating destination blending tradition with modernity.',
    rating: 9.4,
    gallery: [
      require('../assets/Tijuana/8.jpeg'),
      require('../assets/Tijuana/6.jpeg'),
      require('../assets/Tijuana/7.jpeg'),
      require('../assets/Tijuana/5.jpeg'),
    ],
    reviews: [REVIEWS[2], REVIEWS[1]],
    hotels: [HOTELS[1], HOTELS[2], HOTELS[3]],
    type: 'PLACE',
  },
  {
    id: 4,
    image: require('../assets/Tecate/1.jpeg'),
    title: 'Tecate',
    location: 'Baja California',
    description:
      'Tecate, a serene town in Baja California, Mexico, is celebrated for its scenic beauty, including mountains and forests. Home to a famous brewery, it offers a tranquil escape and opportunities for outdoor activities like hiking. ',
    rating: 8.9,
    gallery: [
      require('../assets/Tecate/6.jpeg'),
      require('../assets/Tecate/3.jpeg'),
      require('../assets/Tecate/7.jpeg'),
    ],
    reviews: [REVIEWS[1], REVIEWS[2]],
    hotels: [HOTELS[7], HOTELS[8]],
    type: 'PLACE',
  },
  {
    id: 6,
    image: require('../assets/Ensenada/9.jpeg'),
    title: 'Ensenada',
    location: 'Baja California',
    description:
      "Ensenada, located in Baja California, Mexico, is a coastal city renowned for its stunning ocean views, vibrant cultural scene, and world-class cuisine. As a popular tourist destination, it offers an array of attractions, including beautiful beaches, wineries, and marine life. ",
    rating: 7.4,
    gallery: [
      require('../assets/Ensenada/3.jpeg'),
      require('../assets/Ensenada/2.jpeg'),
      require('../assets/Ensenada/6.jpeg'),
    ],
    reviews: [REVIEWS[1], REVIEWS[2]],
    hotels: [HOTELS[4], HOTELS[5], HOTELS[6]],
    type: 'PLACE',
  },
  {
    id: 11,
    image: require('../assets/Rosarito/4.jpeg'),
    title: 'Rosarito',
    location: 'Baja California',
    description:
      "Rosarito, in Baja California, Mexico, is a coastal town famous for its beaches, entertainment, and seafood. With a lively atmosphere and proximity to the US border, it's a popular destination for relaxation and fun by the Pacific Ocean.",
    rating: 7.4,
    gallery: [
      require('../assets/Rosarito/9.jpeg'),
      require('../assets/Rosarito/11.jpeg'),
      require('../assets/Rosarito/10.jpeg'),
      
    ],
    reviews: [REVIEWS[1], REVIEWS[2]],
    hotels: [HOTELS[9], HOTELS[10], HOTELS[11]],
    type: 'PLACE',
  },
];

export const PLACES = [
  {
    id: 1,
    image: require('../assets/Tijuana/1.jpeg'),
    title: 'Tijuana',
    location: 'Baja California',
    description:
    'Tijuana, in Baja California, Mexico, is a lively border city known for its vibrant nightlife, diverse food scene, and proximity to the US border. It boasts beaches, emerging art, and a rich history, making it a captivating destination blending tradition with modernity.',
    rating: 9.4,
    gallery: [
      require('../assets/Tijuana/8.jpeg'),
      require('../assets/Tijuana/6.jpeg'),
      require('../assets/Tijuana/7.jpeg'),
      require('../assets/Tijuana/5.jpeg'),
    ],
    reviews: [REVIEWS[2], REVIEWS[1]],
    hotels: [HOTELS[1], HOTELS[2], HOTELS[3]],
    type: 'PLACE',
  },
  {
    id: 4,
    image: require('../assets/Tecate/1.jpeg'),
    title: 'Tecate',
    location: 'Baja California',
    description:
      'Tecate, a serene town in Baja California, Mexico, is celebrated for its scenic beauty, including mountains and forests. Home to a famous brewery, it offers a tranquil escape and opportunities for outdoor activities like hiking. ',
    rating: 8.9,
    gallery: [
      require('../assets/Tecate/6.jpeg'),
      require('../assets/Tecate/3.jpeg'),
      require('../assets/Tecate/7.jpeg'),
    ],
    reviews: [REVIEWS[1], REVIEWS[2]],
    hotels: [HOTELS[7], HOTELS[8]],
    type: 'PLACE',
  },
  {
    id: 6,
    image: require('../assets/Ensenada/9.jpeg'),
    title: 'Ensenada',
    location: 'Baja California',
    description:
      "Ensenada, located in Baja California, Mexico, is a coastal city renowned for its stunning ocean views, vibrant cultural scene, and world-class cuisine. As a popular tourist destination, it offers an array of attractions, including beautiful beaches, wineries, and marine life. ",
    rating: 7.4,
    gallery: [
      require('../assets/Ensenada/3.jpeg'),
      require('../assets/Ensenada/2.jpeg'),
      require('../assets/Ensenada/6.jpeg'),
    ],
    reviews: [REVIEWS[1], REVIEWS[2]],
    hotels: [HOTELS[4], HOTELS[5], HOTELS[6]],
    type: 'PLACE',
  },
  {
    id: 11,
    image: require('../assets/Rosarito/4.jpeg'),
    title: 'Rosarito',
    location: 'Baja California',
    description:
      "Rosarito, in Baja California, Mexico, is a coastal town famous for its beaches, entertainment, and seafood. With a lively atmosphere and proximity to the US border, it's a popular destination for relaxation and fun by the Pacific Ocean.",
    rating: 7.4,
    gallery: [
      require('../assets/Rosarito/9.jpeg'),
      require('../assets/Rosarito/11.jpeg'),
      require('../assets/Rosarito/10.jpeg'),
      
    ],
    reviews: [REVIEWS[1], REVIEWS[2]],
    hotels: [HOTELS[9], HOTELS[10], HOTELS[11]],
    type: 'PLACE',
  },
  
];

export const SEARCH_PLACES = [...TOP_PLACES].map(item => ({
  ...item,
  id: Math.random().toString(),
}));

export const SEARCH_HOTELS = [...Object.values(HOTELS)].map(item => ({
  ...item,
  id: Math.random().toString(),
}));

export const SEARCH_ALL = [...SEARCH_PLACES, ...SEARCH_HOTELS];